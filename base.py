# -*- coding: utf-8 -*-
# @Time : 2022/10/18 0:54
# @Script_introduce : pip freeze > requirements.txt   pip install - r requirements.txt
# @Runtime_environment : python3版本 pip install requirement.txt

from common.utils import check_file, timestamp_to_string
from common.error import UserExistsError, RoleError, LevelError, NegativeNumberError, CountError
from common.consts import ROLES, First_Level, Second_Level
import os
import json
import time

"""
    1. 导入user.json    文件检查
    2. 导入gifts.json   文件检查
"""
"""
    1.确定用户表中的信息字段
        username
        role: user or admin
        active:True or False
        create_time: timestamp
        update_time: timestamp
        gifts: []
    2.读取user_json 文件
    3.写入user_json 文件(用户名不重复，存在则不写入)
"""
"""
    1.role 的修改
    2.active 的修改
    3.delete_user 的修改
"""
"""
    1.gifts奖品结构的确定，初始化（三层字典）
        data = {
            "level1": {
                "gift1": {
                    key:{"name": "","count": ""},
                    key:{"name": "","count": ""}
                },
                "gift2": {},
                "gift3": {},
            },
            "level2": {
                "gift1": {},
                "gift2": {},
                "gift3": {},
            },
            "level3": {
                "gift1": {},
                "gift2": {},
                "gift3": {},
            },
            "level4": {
                "gift1": {},
                "gift2": {},
                "gift3": {},
            }
        }
    2.gifts奖品的读取
    3.gifts奖品的添加
    4.gifts奖品的修改
    5.gifts奖品的删除
"""


class Base(object):
    def __init__(self, user_json_path, gifts_json_path):
        self.user_json_path = user_json_path
        self.gifts_json_path = gifts_json_path

        self.__check_user_json()
        self.__check_gifts_json()
        self.__init_gifts()
        # print(self.__read_users())  # 私有函数内部子调用
        # self.__write_user(username="jack", role="admin")  # 验证用户名唯一值是否生效
        # self.__write_user(username="candy", role="admin")  # 验证正常数据是否能更新成功
        # print("写入成功后的数据:{}".format(self.__read_users()))
        print("类{Base}实例化成功")

    def __check_user_json(self):
        check_file(self.user_json_path)

    def __check_gifts_json(self):
        check_file(self.gifts_json_path)

    def __read_users(self, time_to_str=False):
        # def __read_users(self, time_to_str=True):
        with open(self.user_json_path, "r", encoding="utf-8") as f:
            data = json.loads(f.read())  # key:value-->{}
        if time_to_str:
            for username, v in data.items():
                v["create_time"] = timestamp_to_string(timestamp=v["create_time"])
                v["update_time"] = timestamp_to_string(timestamp=v["update_time"])
                data[username] = v  # 修改data 队友的key(username) 的 value(字典)
        # print("读取到的user_json文件数据是:{}".format(data))
        return data

    def __register_user(self, **user):
        if "username" not in user:
            raise ValueError("missing username")
        user["lottery_times"] = 3
        user["role"] = "normal"
        user["active"] = False  # 用户主动注册的为 False  需管理员审核条件
        user["create_time"] = time.time()
        user["update_time"] = time.time()
        user["gifts"] = []

        users = self.__read_users()
        # print("已存在的用户信息:{}".format(users))
        # return
        if user["username"] in users:
            raise UserExistsError("username:{} had exists".format(user["username"]))

        users.update(
            {user["username"]: user}
        )

        self.__save(data=users, path=self.user_json_path)

    # def __write_user(self, **user):
    def write_user(self, **user):
        if "username" not in user:
            raise ValueError("missing username")
        if "role" not in user:
            raise ValueError("missing role")
        if user["role"] not in ROLES:
            raise RoleError("Not use role:{}".format(user["role"]))
        user["active"] = True
        user["lottery_times"] = 3
        user["create_time"] = time.time()
        user["update_time"] = time.time()
        user["gifts"] = []

        users = self.__read_users()
        # print("已存在的用户信息:{}".format(users))
        # return
        if user["username"] in users:
            raise UserExistsError("username:{} had exists".format(user["username"]))

        users.update(
            {user["username"]: user}
        )

        self.__save(data=users, path=self.user_json_path)

    def __change_user(self, username, role):
        # def change_user(self, username, role):
        users = self.__read_users()
        user_value = users.get(username)  # 根据用户名的key 取出用户的所有字段信息 {"username":{username, role, ···}}
        if not user_value:
            return False
        if role not in ROLES:
            raise RoleError("Not use role:{}".format(role))

        user_value["role"] = role
        user_value["update_time"] = time.time()
        users[username] = user_value  # 更新新的用户数据到文件

        self.__save(data=users, path=self.user_json_path)
        return True

    def __change_lottery_times(self, users, username, userinfo):
        # def change_lottery_times(self, username):
        if not userinfo:
            return False
        # 判断最后的更新时间update_time是不是新的一天日期
        if time.strftime("%Y-%m-%d", time.localtime(userinfo.get("update_time"))) == time.strftime("%Y-%m-%d",
                                                                                                   time.localtime(
                                                                                                           time.time())):
            return
        userinfo["lottery_times"] = 3  # 日期不相同，重置 次数
        userinfo["update_time"] = time.time()
        users[username] = userinfo  # 更新新的用户数据到文件
        self.__save(data=users, path=self.user_json_path)
        return True

    def __change_active(self, username):
        # def change_active(self, username):
        users = self.__read_users()
        user_value = users.get(username)  # 根据用户名的key 取出用户的所有字段信息 {"username":{username, role, ···}}
        if not user_value:
            return False
        user_value["active"] = not user_value["active"]
        user_value["update_time"] = time.time()
        users[username] = user_value  # 更新新的用户数据到文件

        self.__save(data=users, path=self.user_json_path)
        return True

    def __check_active(self, avtive=False):
        users = self.__read_users()
        user_list = []
        for k, userinfo in users.items():
            if userinfo["active"] == avtive:
                user_list.append(userinfo)
        return user_list

    def __delete_user(self, username):
        # def delete_user(self, username):
        users = self.__read_users()
        user_value = users.get(username)  # 根据用户名的key 取出用户的所有字段信息 {"username":{username, role, ···}}
        if not user_value:
            return False

        delete_user = users.pop(username)  # 返回删除了哪个用户的信息

        self.__save(data=users, path=self.user_json_path)
        return delete_user

    def __read_gifts(self):
        with open(self.gifts_json_path, "r", encoding="utf-8") as f:
            data = json.loads(f.read())  # key:value-->{}
        return data

    def __init_gifts(self):
        data = {
            "level1": {
                "gift1": {
                    "books": {"name": "books", "count": 2},
                    "pen": {"name": "pen", "count": 0}
                },
                "gift2": {},
                "gift3": {
                    "pen": {"name": "pen", "count": 1}
                },
            },
            "level2": {
                "gift1": {},
                "gift2": {},
                "gift3": {},
            },
            "level3": {
                "gift1": {},
                "gift2": {},
                "gift3": {},
            },
            "level4": {
                "gift1": {},
                "gift2": {},
                "gift3": {},
            }
        }
        gifts = self.__read_gifts()
        if len(gifts) != 0:
            return
        self.__save(data=data, path=self.gifts_json_path)

    def __check_level_giftsname(self, first_level, second_level, gift_name):
        if first_level not in First_Level:
            raise LevelError("{} 不存在".format(first_level))
        if second_level not in Second_Level:
            raise LevelError("{} 不存在".format(second_level))
        gifts = self.__read_gifts()
        current_gift_pool = gifts[first_level]
        current_second_gift_pool = current_gift_pool[second_level]
        if gift_name not in current_second_gift_pool:
            return {
                "status": False,  # 自己增加的状态
                "level_one": current_gift_pool,
                "level_two": current_second_gift_pool,
                "gifts": gifts
            }
        return {
            "status": True,  # 自己增加的状态
            "level_one": current_gift_pool,
            "level_two": current_second_gift_pool,
            "gifts": gifts
        }

    def __write_gifts(self, first_level, second_level, gift_name, gift_count: int):
        # def write_gifts(self, first_level, second_level, gift_name, gift_count: int):
        # def write_gifts(self, first_level, second_level, gift_name, gift_count: int):
        # if first_level not in First_Level:
        #     raise LevelError("{} 不存在".format(first_level))
        #
        # if second_level not in Second_Level:
        #     raise LevelError("{} 不存在".format(second_level))
        # gifts = self.__read_gifts()
        # current_gift_pool = gifts[first_level]
        # current_second_gift_pool = current_gift_pool[second_level]
        # if gift_name in current_second_gift_pool:
        #     current_second_gift_pool[gift_name]['count'] = current_second_gift_pool[gift_name]['count'] + gift_count
        # else:
        #     current_second_gift_pool[gift_name] = {
        #         'name': gift_name,
        #         'count': gift_count
        #     }
        # current_gift_pool[second_level] = current_second_gift_pool
        # gifts[first_level] = current_gift_pool
        if gift_count <= 0:
            return False
        data = self.__check_level_giftsname(first_level, second_level, gift_name)
        if not data["status"]:
            data["level_two"][gift_name] = {
                'name': gift_name,
                'count': gift_count
            }
        else:
            data["level_two"][gift_name]['count'] = data["level_two"][gift_name]['count'] + gift_count

        data["level_one"][second_level] = data["level_two"]
        data["gifts"][first_level] = data["level_one"]
        self.__save(data=data["gifts"], path=self.gifts_json_path)

    def __update_gifts(self, first_level, second_level, gift_name, gift_count: int = 1, is_admin=False):
        # def update_gifts(self, first_level, second_level, gift_name, gift_count: int = 1):
        # if first_level not in First_Level:
        #     raise LevelError("{} 不存在".format(first_level))
        # if second_level not in Second_Level:
        #     raise LevelError("{} 不存在".format(second_level))
        # gifts = self.__read_gifts()
        # current_gift_pool = gifts[first_level]
        # current_second_gift_pool = current_gift_pool[second_level]
        # if gift_name not in current_second_gift_pool:
        #     return False
        # original_gift_count = current_second_gift_pool[gift_name]["count"]
        # if original_gift_count - gift_count >= 0:
        #     current_second_gift_pool[gift_name]["count"] -= gift_count
        # else:
        #     raise NegativeNumberError("奖品数量:{}不足，无法支持分配此次数量:{}".format(original_gift_count, gift_count))
        # current_gift_pool[second_level] = current_second_gift_pool
        #
        # gifts[first_level] = current_gift_pool
        assert isinstance(gift_count, int), "gift_count is a int"  # 断言
        # if gift_count <= 0:
        #     # return False
        #     raise NegativeNumberError("奖品数量不能为负数或零")

        data = self.__check_level_giftsname(first_level, second_level, gift_name)
        if not data["status"]:
            print("奖品:{}-{}-{} not exists".format(
                first_level, second_level, gift_name
            ))
            return data["status"]
        else:
            # 要修改的奖品存在，在进行身份验证
            if is_admin:
                if gift_count < 0:
                    # return False
                    raise CountError("管理员修改奖品:{},数量:{} 不能为负数".format(gift_name, gift_count))
                print("管理员修改奖品:{},数量:{}".format(gift_name, gift_count))
                data["level_two"][gift_name]["count"] = gift_count
            else:
                if gift_count <= 0:
                    # return False
                    raise NegativeNumberError("奖品数量不能为负数或零")
                original_gift_count = data["level_two"][gift_name]["count"]
                if original_gift_count - gift_count >= 0:
                    data["level_two"][gift_name]["count"] -= gift_count
                else:
                    raise NegativeNumberError(
                        "奖品数量:{}不足,无法支持分配此次数量:{}".format(original_gift_count, gift_count))
            data["level_one"][second_level] = data["level_two"]
            data["gifts"][first_level] = data["level_one"]
            self.__save(data=data["gifts"], path=self.gifts_json_path)

    def __delete_gifts(self, first_level, second_level, gift_name):
        # def delete_gifts(self, first_level, second_level, gift_name):
        # if first_level not in First_Level:
        #     raise LevelError("{} 不存在".format(first_level))
        # if second_level not in Second_Level:
        #     raise LevelError("{} 不存在".format(second_level))
        # gifts = self.__read_gifts()
        # current_gift_pool = gifts[first_level]
        # current_second_gift_pool = current_gift_pool[second_level]
        # if gift_name not in current_second_gift_pool:
        #     return False
        # delete_gift = current_second_gift_pool.pop(gift_name)
        # current_gift_pool[second_level] = current_second_gift_pool
        # gifts[first_level] = current_gift_pool
        data = self.__check_level_giftsname(first_level, second_level, gift_name)
        if data["status"]:
            delete_gift = data["level_two"].pop(gift_name)
            data["level_one"][second_level] = data["level_two"]
            data["gifts"][first_level] = data["level_one"]
            self.__save(data=data["gifts"], path=self.gifts_json_path)
            return delete_gift
        else:
            return data["status"]

    def __save(self, data, path):
        json_data = json.dumps(data)
        with open(path, "w") as f:
            f.write(json_data)
        print("数据写入后的值:{}".format(json_data))


if __name__ == '__main__':
    # user_json_path = os.path.join(os.getcwd(), "storage", "test.py")  # 验证自定义异常类是否生效
    user_json_path = os.path.join(os.getcwd(), "storage", "user.json")
    gifts_json_path = os.path.join(os.getcwd(), "storage", "gifts.json")
    # print(user_json_path)
    # print(gifts_json_path)

    base_obj = Base(user_json_path=user_json_path, gifts_json_path=gifts_json_path)

    result = base_obj.write_user(username="candy", role="normal")
    # result = base_obj.change_user(username="jack", role="normal")
    # result = base_obj.change_active(username="candy")
    # result = base_obj.delete_user(username="jack")

    # result = base_obj.write_gifts(first_level="level1", second_level="gift1", gift_name="pen", gift_count=1)
    # result = base_obj.update_gifts(first_level="level1", second_level="gift1", gift_name="pen", gift_count=2)
    # result = base_obj.delete_gifts(first_level="level1", second_level="gift1", gift_name="books")
    print("操作结果:{}".format(result))
