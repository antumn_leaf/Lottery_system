# -*- coding: utf-8 -*-
# @Time : 2022/10/18 0:54
# @Script_introduce : 
# @Runtime_environment : python3版本 pip install requirement.txt
from common.error import NotUserError, UserActiveError, RoleError
from common.utils import timestamp_to_string

from base import Base

import os

"""
    1.admin 类的搭建
    2.获取用户函数（都要先判断是否是admin身份）
    3.添加用户
    4.冻结与恢复用户:active
    5.修改用户身份
"""
"""
    1.admin 的验证 只有admin 才有权限更改后端数据
    2.动态更新getuser 实时状态
    3.奖品gifts 的添加、删除、修改(修改base)
"""


class Admin(Base):
    def __init__(self, username, user_json_path, gifts_json_path):
        self.username = username
        super().__init__(user_json_path=user_json_path, gifts_json_path=gifts_json_path)
        print("类{Admin}实例化成功")
        self.get_user()

    def get_user(self):
        users = self._Base__read_users()
        # print(users)
        current_user = users.get(self.username)
        if not current_user:
            raise NotUserError("user:{} not exists".format(self.username))
        if not current_user.get("active"):
            raise UserActiveError("this user active is :{}".format(current_user.get("active")))
        if current_user.get("role") != "admin":
            raise RoleError("{} is not admin".format(current_user.get("username")))
        self.name = current_user.get("username")
        self.role = current_user.get("role")
        self.lottery_times = current_user.get("lottery_times")
        self.active = current_user.get("active")
        self.create_time = current_user.get("create_time")

        try:
            print(
                "获取到的用户信息:name:{}, role:{}, active:{}, create_time:{}".format(self.name, self.role, self.active,
                                                                                      timestamp_to_string(
                                                                                          self.create_time)))
        except:
            print(
                "获取到的用户信息:name:{}, role:{}, active:{}, create_time:{}".format(self.name, self.role, self.active,
                                                                                      self.create_time))

    def __check_admin(self, message):
        self.get_user()
        if self.role != "admin":
            raise Exception(message)

    def add_user(self, username, role):
        # self.get_user()
        # if self.role != "admin":
        #     raise Exception("Permission not allowed")
        self.__check_admin("Permission not allowed")

        self._Base__write_user(username=username, role=role)

    def check_user_active(self, active=False):
        # self.get_user()
        # if self.role != "admin":
        #     raise Exception("Permission not allowed")
        self.__check_admin("Permission not allowed")
        result = self._Base__check_active(active)
        print("所有符合条件active:{} 数据为:{}条,值:{}".format(active, len(result), result))

    def update_user_active(self, username):
        # self.get_user()
        # if self.role != "admin":
        #     raise RoleError("Permission not allowed")
        self.__check_admin("Permission not allowed")
        self._Base__change_active(username=username)

    def update_user_info(self, username, role):
        # self.get_user()
        # if self.role != "admin":
        #     raise RoleError("Permission not allowed")
        self.__check_admin("Permission not allowed")
        self._Base__change_user(username=username, role=role)

    def add_gifts(self, first_level, second_level, gift_name, gift_count):
        # self.get_user()
        # if self.role != "admin":
        #     raise RoleError("Permission not allowed")
        self.__check_admin("Permission not allowed")
        self._Base__write_gifts(first_level=first_level, second_level=second_level, gift_name=gift_name,
                                gift_count=gift_count)

    def delete_gifts(self, first_level, second_level, gift_name):
        # self.get_user()
        # if self.role != "admin":
        #     raise RoleError("Permission not allowed")
        self.__check_admin("Permission not allowed")
        self._Base__delete_gifts(first_level, second_level, gift_name)

    def update_gifts(self, first_level, second_level, gift_name, gift_count):
        # self.get_user()
        # if self.role != "admin":
        #     raise RoleError("Permission not allowed")
        self.__check_admin("Permission not allowed")
        self._Base__update_gifts(first_level, second_level, gift_name, gift_count, is_admin=True)


if __name__ == '__main__':
    user_json_path = os.path.join(os.getcwd(), "storage", "user.json")
    gifts_json_path = os.path.join(os.getcwd(), "storage", "gifts.json")
    # print(user_json_path)
    # print(gifts_json_path)

    admin_obj = Admin(username="jack99", user_json_path=user_json_path, gifts_json_path=gifts_json_path)
    # admin_obj.add_user(username="mark", role="normal")
    # admin_obj.update_user_active(username="mark")
    # admin_obj.check_user_active(active=True)
    # admin_obj.update_user_info(username="candy", role="admin")
    # admin_obj.update_user_info(username="candy", role="normal")
    # admin_obj.add_gifts(first_level="level1", second_level="gift1", gift_name="pen", gift_count=2)
    # admin_obj.delete_gifts(first_level="level1", second_level="gift1", gift_name="pen")
    # admin_obj.update_gifts(first_level="level1", second_level="gift1", gift_name="pen", gift_count=2)
    # admin_obj.update_gifts(first_level="level1", second_level="gift1", gift_name="pen", gift_count=6)
