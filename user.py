# -*- coding: utf-8 -*-
# @Time : 2022/10/18 0:54
# @Script_introduce : 
# @Runtime_environment : python3版本 pip install requirement.txt
import os
import random
from base import Base

from common.error import NotUserError, RoleError, UserActiveError
from common.utils import timestamp_to_string

"""
    1.user 类的初始化
    2.获取用户（getuser）
    3.用户注册
    4.查看奖品列表
"""
"""
    1.防止并发操作 user_json_path gifts_json_path 多进程枷锁，释放锁，写在 try: lock except finally: release --中
    2.用户与管理员的有效性验证:(password, active=True) 
    3.每日抽奖次数限制
"""
"""
    1.抽奖环节:
        随机第一层（level1） 50% 30% 15% 5%
        随机第二层（level2） 70% 25% 5%
        取到对应层级的资格后判断奖池该奖品是否还有 count>0,有则中将并提示用户 count-1 用户gifts添加奖品信息，无则未中奖
"""


class User(Base):
    def __init__(self, username, user_json_path, gifts_json_path):
        self.username = username
        self.random_num = list(range(1, 101))
        super().__init__(user_json_path=user_json_path, gifts_json_path=gifts_json_path)
        print("类{User}实例化成功")
        self.get_user()

        # self.get_gifts()

    def get_user(self):
        users = self._Base__read_users()
        if self.username not in users:
            raise NotUserError("username:{} is not in user_json".format(self.username))
        self.current_user = users.get(self.username)  # 取到用户基本信息{username, role}
        if not self.current_user.get("active"):
            raise UserActiveError("user:{} active is :{}".format(self.username, self.current_user.get("active")))
        if self.current_user.get("role") != "normal":
            raise RoleError("{} is not a normal user".format(self.username))
        # self.name = self.current_user.get("username")
        # self.role = self.current_user.get("role")
        # self.lottery_times = self.current_user.get("lottery_times")
        # self.gifts = self.current_user.get("gifts")
        # self.create_time = self.current_user.get("create_time")
        # print("用户基本信息,username:{} role:{} create_time:{} gifts:{}"
        #       .format(self.name, self.role, timestamp_to_string(self.create_time), self.gifts))
        self._Base__change_lottery_times(users, self.username, self.current_user)
        print("用户基本信息:{}".format(self.current_user))

    def register_user(self, username):
        self._Base__register_user(username=username)

    def get_gifts(self):
        gifts = self._Base__read_gifts()
        gifts_list = []
        for level_one, level_one_pool in gifts.items():
            for level_two, level_two_pool in level_one_pool.items():
                for gift_name, gift_info in level_two_pool.items():
                    # print("gift_name:{}, gift_info:{}".format(gift_name, gift_info))
                    gifts_list.append(gift_name)
        print("奖池剩余礼品信息:{}".format(gifts_list))

    def choice_gifts(self):
        self.get_user()
        if self.current_user.get("lottery_times") > 0:
            self.current_user["lottery_times"] -= 1
            first_level, second_level = None, None
            level1_num = random.choice(self.random_num)
            if 1 <= level1_num <= 50:
                first_level = "level1"
            elif 51 <= level1_num <= 80:
                first_level = "level2"
            elif 81 <= level1_num <= 90:
                first_level = "level3"
            elif 91 <= level1_num <= 100:
                first_level = "level4"
            gifts = self._Base__read_gifts()
            level_one_pool = gifts.get(first_level)
            print("随机值:{},获得抽取:{} 资格".format(level1_num, level_one_pool))

            level2_num = random.choice(self.random_num)
            if 1 <= level2_num <= 70:
                second_level = "gift1"
            elif 71 <= level2_num <= 95:
                second_level = "gift2"
            elif 96 <= level2_num <= 100:
                second_level = "gift3"
            level_two_pool = level_one_pool.get(second_level)
            print("随机值:{},获得抽取:{} 资格".format(level2_num, level_two_pool))
            if len(level_two_pool) == 0:
                print("奖池奖品已兑完，无法中奖了")
                self.update_user()
                return
            level_two_pool_gifts_name = []
            for gift_name, _ in level_two_pool.items():
                level_two_pool_gifts_name.append(gift_name)

            winning_gift_name = random.choice(level_two_pool_gifts_name)  # pen
            gift_info = level_two_pool.get(winning_gift_name)  # {"name": "pen", "count": 6}
            if gift_info.get("count") <= 0:
                print("奖池奖品已兑完，无法中奖了")
                self.update_user()
                return
            print("winning_gift_name:{}".format(winning_gift_name))
            # gift_info["count"] -= 1
            # level_two_pool[winning_gift_name] = gift_info  # {"name": "pen", "count": 5}
            # level_one_pool[second_level] = level_two_pool
            # gifts[first_level] = level_one_pool
            # self._Base__save(gifts, self.gifts_json_path)

            self._Base__update_gifts(first_level, second_level, winning_gift_name, gift_count=1)

            self.current_user["gifts"].append(winning_gift_name)
            self.update_user()
        else:
            print("用户:{},抽奖次数已用光,无法抽奖".format(self.current_user.get("username")))

    def update_user(self):
        users = self._Base__read_users()
        users[self.username] = self.current_user
        self._Base__save(users, self.user_json_path)


if __name__ == '__main__':
    user_json_path = os.path.join(os.getcwd(), "storage", "user.json")
    gifts_json_path = os.path.join(os.getcwd(), "storage", "gifts.json")
    # print(user_json_path)
    # print(gifts_json_path)

    # user_obj = User(username="jack", user_json_path=user_json_path, gifts_json_path=gifts_json_path)  # 管理员
    user_obj = User(username="jack", user_json_path=user_json_path, gifts_json_path=gifts_json_path)  # 用户 True
    # user_obj.register_user(username="muke")
    # user_obj = User(username="mark", user_json_path=user_json_path, gifts_json_path=gifts_json_path)  # 用户 False
    user_obj.choice_gifts()
