# -*- coding: utf-8 -*-
# @Time : 2022/10/18 0:50
# @Script_introduce : pip install mysql-connector-python  pip install --upgrade pip   *****  pip install colorama
# cmd 下 python -m pip install --upgrade pip
# @Runtime_environment : python3版本 pip install requirement.txt

from error import NotFileError, NotFilePathError, FormatError
from colorama import Back, Fore, Style
import os
import time
import logging
import mysql.connector.pooling


def check_file(path):
    if not os.path.exists(path):
        raise NotFilePathError("Not Find {}".format(path))

    if not os.path.isfile(path):
        raise NotFileError("this is not file :{}".format(path))

    if not path.endswith(".json"):
        raise FormatError("Need json format")


def timestamp_to_string(timestamp):
    time_obj = time.localtime(timestamp)
    time_str = time.strftime("%Y-%m-%d %H:%M:%S", time_obj)
    return time_str


def init_mysql_connect_pool():
    config = {
        "host": "127.0.0.1",
        "port": 3306,
        "user": "root",
        "password": "123456",
        "database": ""
    }
    try:
        conn_pool = mysql.connector.pooling.MySQLConnectionPool(
            **config,
            pool_size=5
        )
        conn = conn_pool.get_connection()
        conn.start_transaction()

        cursor = conn.cursor()
        sql = "INSERT INTO STUDENTS (sno,sname,sage,ssex)" \
              "values (%s,%s,%s,%s)"  # 使用占位符，后面执行再提供数据

        cursor.execute(sql, (3168907218, "AFeng", 22, "男"))  # 采用预编译抵御sql注入攻击

        conn.commit()
    except Exception as e:
        if "conn" in dir():
            conn.rollback()
        print(Fore.LIGHTRED_EX, "失败原因:{}".format(e))


def log_init():
    time_string = time.strftime("%Y-%m-%d", time.localtime(time.time()))  # 日志按天归档在logs 文件夹下
    case_logger = logging.getLogger("collect")  # 会创建一个Logger对象
    case_logger.setLevel("INFO")
    console_handle = logging.StreamHandler()  # Handler对象
    file_handle = logging.FileHandler("logs/CollectMembers_{}.log".format(time_string), encoding="utf-8")
    console_handle.setLevel(logging.INFO)
    file_handle.setLevel(logging.INFO)
    verbose_formatter = logging.Formatter('[%(asctime)s] - [%(levelname)s] - [%(lineno)d] - [msg]: [%(message)s]')
    console_handle.setFormatter(verbose_formatter)
    file_handle.setFormatter(verbose_formatter)
    case_logger.addHandler(console_handle)
    case_logger.addHandler(file_handle)
    return case_logger

# if __name__ == '__main__':
#     print("当前时间:{}".format(timestamp_to_string(timestamp=time.time())))
#     print(Fore.LIGHTBLUE_EX, "HelloWord")  # 字体颜色
#     print(Back.LIGHTWHITE_EX, "HelloWord")  # 背景颜色
#     print(Style.RESET_ALL, "HelloWord")  # 重置所有颜色
